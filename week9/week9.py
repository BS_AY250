#!/usr/bin/env python

'''
AY250 HW9
Parallel processing shoot out
Author: Bryan Steinbach
'''

import numpy as np
from time import time
import threading
from multiprocessing import Process,Value

nproc = 2

def exec_time(f):
	''' Benchmarking decorator '''
	def wrapper(*args,**kwargs):
		start_time = time()
		f(*args,**kwargs)
		end_time = time()
		return end_time - start_time

	return wrapper

def algorithm(ndart):
	''' Core algorithm to test, used by all methods
	Ignores rounding error in blocksize '''
	blocksize = 4096
	nblocks = ndart / blocksize

	in_circle = 0
	for i in range(nblocks):
		xs,ys = np.random.uniform(-0.5,0.5,size=blocksize*2).reshape((2,blocksize))
		rsquared = xs**2+ys**2
		in_circle += np.sum(rsquared <= 0.25)

	return in_circle

def to_pi(in_circle,ndart):
	''' Estimate pi from count in circle '''
	return 4*in_circle/float(ndart)

@exec_time
def serial_test(ndart):
	''' Direct serial test algorithm '''
	print "serial_test: ",ndart
	in_circle = algorithm(ndart)
	pi_approx= to_pi(in_circle,ndart)

class PiThread(threading.Thread):
	def __init__(self,ndart):
		threading.Thread.__init__(self)
		self.ndart = ndart
	def run(self):
		self.in_circle = algorithm(self.ndart)

@exec_time
def thread_test(ndart):
	''' threaded version '''
	print "thread_test: ",ndart
	nthread = 2*nproc
	ndarthread = ndart / nthread

	threads = [PiThread(ndarthread) for i in range(nthread)]
	for thread in threads: thread.start()
	for thread in threads: thread.join()

	in_circle = np.sum([thread.in_circle for thread in threads])
	pi_approx = to_pi(in_circle,ndart)

class PiProcess(Process):
	def __init__(self,ndart):
		Process.__init__(self)
		self.ndart = ndart
		self.ret = Value('i',0)
	def run(self):
		self.ret.value = algorithm(self.ndart)

@exec_time
def multi_test(ndart):
	''' multiprocessing version '''
	print "multi_test: ",ndart
	nthread = nproc
	ndarthread = ndart / nthread

	threads = [PiProcess(ndarthread) for i in range(nthread)]
	for thread in threads: thread.start()
	for thread in threads: thread.join()

	in_circle = np.sum([thread.ret.value for thread in threads])
	pi_approx = to_pi(in_circle,ndart)

if __name__ == '__main__':
	ns = np.int_(np.logspace(5,7.5,20))

	s = np.array([serial_test(n) for n in ns])
	t = np.array([thread_test(n) for n in ns])
	m = np.array([multi_test(n) for n in ns])

	import pylab as pl
	pl.subplot(211)
	pl.title('Comparison of Parallel Computing Execution Times')
	pl.loglog(ns,s,label='serial')
	pl.loglog(ns,t,label='thread')
	pl.loglog(ns,m,label='multi')
	pl.grid()
	pl.ylabel('Execution time (s)')
	pl.legend(loc='upper left')

	pl.subplot(212)
	pl.semilogx(ns,ns/s,label='serial')
	pl.semilogx(ns,ns/t,label='thread')
	pl.semilogx(ns,ns/m,label='multi')
	pl.grid()
	pl.xlabel('Darts thrown')
	pl.ylabel('Rate (Hz)')
	pl.savefig('comparison.png')
	pl.show()

