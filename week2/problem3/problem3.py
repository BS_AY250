'''
AY250 HW2 Problem 3
Construct matrix inverse
Author: Bryan Steinbach
'''

import numpy as np
from scipy import linalg
import pylab as pl

class NonSquareMatrix(Exception):
	pass

class SingularMatrix(Exception):
	pass

def inv(mat,tol=1e-6):
	n = mat.shape[0]
	if n != mat.shape[1]:
		raise NonSquareMatrix

	det = linalg.det(mat)
	if abs(det) < tol:
		raise SingularMatrix
		
	# Fortunately the problem statement doesn't specify that we need to implement Gauss elimination
	# Use scipy to build the LU decomposition, then extract the matrix inverse by solving for the basis vectors
	lu,piv = linalg.lu_factor(mat)

	matinv = np.zeros((n,n))

	for i in range(n):
		v = np.zeros(n)
		v[i] = 1

		matinv[:,i] = linalg.lu_solve((lu,piv),v)

	return matinv

# Test with a 200x200 random matrix
nt = 200

m = np.random.randn(nt*nt).reshape((nt,nt))

mi = inv(m)

id = np.dot(m,mi)

resid = (id - np.eye(nt)).flatten()

def rms(x): return np.sqrt(np.dot(x,x)/x.size)
rmsresid = rms(resid)
print "RMS residual: ",rmsresid

pl.hist(resid,bins=100,log=True)
pl.ylabel('Count')
pl.xlabel('Residual')
pl.title('Residual histogram of $I - M \cdot M^{-1}$, RMS=%0.3g'%(rmsresid))
pl.savefig('resid-hist.png')
