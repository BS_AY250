'''
AY250 HW2 Problem 2
Matched filtering
Author:  Bryan Steinbach

Strategy:  Construct radial power spectrum, find the radial frequencies where the spikes are lying, and mask those out.  Mask out the first few individually, then throw the rest out whole sale when they get dense.  Image power follows a 1/f^a model, so we don't lose much signal doing this.
'''

import numpy as np
import pylab as pl
from scipy import weave

imgfn = 'moonlanding.png'

img = pl.imread(imgfn)*1.0 # Read and convert to doubles

print "shape: ",img.shape
# (474,630)
# So this is already a gray scale image

'''
pl.imshow(img,cmap='gray')
pl.show()
'''

# Compute 2d fourier transform, normalized 2d amplitude spectral density, and 1d radial asd
fimg = np.fft.fft2(img)
fimg_asd = np.abs(fimg)
fimg_psd = fimg_asd**2

fimg_asd_norm = fimg_asd/np.max(fimg_asd)

fimg_asd_db = 10*np.log10(fimg_asd_norm)

# Strategy - make a signal and noise model, and a matched filter to maximize one and reject the other

'''
pl.imshow(fimg_asd_db,cmap='gray')
pl.colorbar()
pl.show()
'''

nx = fimg.shape[0]
ny = fimg.shape[1]

# 2d versions of fftfreq
kx = np.tile(np.fft.fftfreq(nx,1),(ny,1)).transpose()
ky = np.tile(np.fft.fftfreq(ny,1),(nx,1))
kr = np.sqrt(kx**2+ky**2)		# Radial frequencies

# Make a 1d radial power spectrum by binning
krmin = 0.0
krmax = np.max(kr)

nbins = 256
psd1d = np.zeros(nbins)
psd1d_hits = np.zeros(nbins,dtype=np.int)

# Radial frequency bins of fourier transformed image
kimg = np.int_((nbins-1)*(kr-krmin)/(krmax-krmin))

# Radial frequencies of output spectrum
kbin = np.linspace(krmin,krmax,nbins)

# From looking at this plot I find these masks
maskout = np.zeros(fimg.shape,dtype=np.bool)
maskout |= (kr > 0.148)
maskout |= (kr > 0.096) & (kr < 0.1)
maskout |= (kr > 0.113) & (kr < 0.118)
maskout |= (kr > 0.133) & (kr < 0.140)

fimg_psd[maskout] = 0.

# Build mask by looking for spikes in the radial frequency spectrum
def build_mask():
	# I'd like to write
	# bins[kbin] += fimg_psd
	# But the += semantics are wrong for that - behaves like bins[kbin] = bins[kbin] + fimg_psd and assigns with out updating
	# Previously I've hacked around this using sparse arrays, but for a single small image there's no need to optimize
	for i in range(fimg.shape[0]):
		for j in range(fimg.shape[1]):
			psd1d[kimg[i,j]] += fimg_psd[i,j]
			psd1d_hits[kimg[i,j]] += 1

	ok = psd1d_hits > 0
	psd1d[ok] /= psd1d_hits[ok]

	asd1d = np.sqrt(psd1d)

	ok = asd1d > 0

	pl.loglog(kbin[ok][1:],asd1d[ok][1:])
	pl.show()

#build_mask()

fimg[maskout] = 0.

filtered_img = np.fft.ifft2(fimg).real

delta_img = filtered_img - img

#pl.imshow(filtered_img,cmap='gray')
pl.imsave('filtered.png',filtered_img,cmap='gray')
pl.imsave('resid.png',delta_img,cmap='gray')


