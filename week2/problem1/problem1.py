'''
AY250 HW2 Problem 1
Fitting to noisy data
Author:  Bryan Steinbach
'''


import numpy as np
import scipy as sp
from scipy import optimize,interpolate
import pylab as pl

# Problem statement doesn't specify how many points or where they should go, beyond between 0 and 4
# Pick 100 points evenly spaced
n = 100

def model(p,x):
	return p[0]*np.exp(p[1]*x)+p[2]

sigma = 0.1

x = np.linspace(0,4,n)

def rms(x): return np.sqrt(np.dot(x,x)/x.size)

# The measurements
ptrue = np.array([2,-0.75,0.1])	# a, alpha, k
ynoise = sigma*np.random.randn(n)
print "true parameters: ",ptrue
print "noise standard deviation: ",np.std(ynoise), "expected ",sigma
ysignal = model(ptrue,x)
yd = ysignal+ynoise

# The analysis
# Least squares fit
p0 = np.array([1.,0.,min(yd)])

p,ier = optimize.leastsq(lambda p: model(p,x) - yd,p0)

yleastsq = model(p,x)

print "sp.optimize.leastsq ier: ",ier

print "leastsq parameters: ",p

print "leastsq rms error: ",rms(ysignal - yleastsq)
print "leastsq rms resid: ",rms(yd - yleastsq)

# spline smooth
tck = interpolate.splrep(x,yd,s=2.0)
yspline = interpolate.splev(x,tck)

print "spline rms error: ",rms(ysignal - yspline)
print "spline rms resid: ",rms(yd - yspline)

# poly fits
npoly=3
ypolys = []
for i in range(npoly):
	poly = np.polyfit(x,yd,i)

	ypoly = np.polyval(poly,x)
	ypolys.append(ypoly)

	print "poly%d rms error: "%i,rms(ysignal - ypoly)
	print "poly%d rms resid: "%i,rms(yd - ypoly)

	
# Plot results
pl.plot(x,ysignal,label="signal")
pl.plot(x,yd,label="signal+noise")
pl.plot(x,yleastsq,label="least squares fit")
pl.plot(x,yspline,label='spline smooth')
for i in range(npoly):
	pl.plot(x,ypolys[i],label='poly%d'%i)
pl.grid()
pl.xlabel('x')
pl.ylabel('y')
pl.legend()
pl.title('Fitting to $y = a \\exp(\\alpha x) + k + n(x)$')

pl.savefig('fits.png')

