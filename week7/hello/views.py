
import tempfile

from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render_to_response
from django import forms
from django.template import Template

import database


### Home page

def render_collections():
	''' Generate HTML for list of collections for home page '''
	cs = database.get_collections()

	if not cs:
		return '<p>No collections in database'

	s = '<p> Available collections: '
	for c in cs:
		s += '<p> '+c
	
	return s


def index(request):
	''' Generate HTML for home page '''
	collections = render_collections()
	page = '''
	<html>
	<body>

	<p>
	<a href="/insert_collection/">Insert collection</a>

	<p>
	<a href="/query/">Query collections</a>

	%s

	</body>
	</html>
	'''%collections


	return HttpResponse(page)


### Upload page

class UploadFileForm(forms.Form):
	title = forms.CharField(max_length=50)
	file  = forms.FileField()

def handle_uploaded_file(title,upload_data):
	print "handle uploaded file: ",title,upload_data

	with tempfile.TemporaryFile() as tf:
		for chunk in upload_data.chunks():
			tf.write(chunk)
		tf.seek(0)
		database.insert_bibtex_file(title,tf)

fileform = '''
<form enctype="multipart/form-data" method="post" action="/insert_collection.html">
	<p><label for="title">Collection name:</label>
	<input id="title" type="text" name="title" maxlength="100" /></p>

	<p><label for="file">Bibtex file:</label>
	<input type="file" name="file"/>
	<input type="submit" value="Upload!" />

</form>
'''

def upload_file(request):
	''' Allow user to upload new bibtex collection files
	Reached at insert_collection.html '''
	if request.method == 'POST':
		print "POST requested"
		# Path taken after user hits upload button
		form = UploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			file = request.FILES['file']
			title = form.cleaned_data['title']
			handle_uploaded_file(title,file)
			return HttpResponseRedirect('/insert_success.html')
		else:
			return HttpResponseRedirect('/insert_failure.html')
	else:
		# Path taken when user first arrives
		# This could be used in the fileform template
		form = UploadFileForm()
#	return render_to_response(fileformtemplate, {'form': form})
	return HttpResponse(fileform)

def upload_success(request):
	page = '''
	<html>
	<body>
	Insert successful
	<p>
	<a href="/">Return to main</a>
	</body>
	</html>
	'''
	return HttpResponse(page)

def upload_failure(request):
	page = '''
	<html>
	<body>
	Insert failed
	<p>
	<a href="/">Return to main</a>
	</body>
	</html>
	'''
	return HttpResponse(page)


### Search page

class QueryForm(forms.Form):
	query = forms.CharField(max_length=50)

fileform = '''
<form method="post" action="/query.html">
	<p><label for="query">Query: <label>
	<input id="query" type="text" name="query" maxlength="100" /></p>

	<input type="submit" value="Go!" />

</form>
'''

def handle_query(query):
	return ""

def query(request):
	''' Allow user to query the database '''
	if request.method == 'POST':
		print "POST requested"
		# Path taken after user hits upload button
		form = UploadFileForm(request.POST, request.FILES)
		if form.is_valid():
			file = request.FILES['file']
			title = form.cleaned_data['title']
			handle_uploaded_file(title,file)
			return HttpResponseRedirect('/insert_success.html')
		else:
			return HttpResponseRedirect('/insert_failure.html')
	else:
		# Path taken when user first arrives
		# This could be used in the fileform template
		form = UploadFileForm()
#	return render_to_response(fileformtemplate, {'form': form})
	return HttpResponse(fileform)
