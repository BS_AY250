
from django.conf.urls.defaults import *
urlpatterns = patterns('',
		(r'insert_collection', 'views.upload_file'),
		(r'insert_success', 'views.upload_success'),
		(r'insert_failure', 'views.upload_failure'),
		(r'query','views.query'),
		(r'^$', 'views.index'),
		)


