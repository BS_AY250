
import os
import sqlite3

import bibparse

tablefn = 'bibtex_database'
table = 'bibtex'

conn = sqlite3.connect(tablefn)

columns = ['Author','Journal','Pages','DateModified','Title','Collection']

def create_table():
	c = conn.cursor()

	typelist = ', '.join([column+' text' for column in columns])

	create_stmt = 'create table %s (%s)'%(table,typelist)

	c.execute(create_stmt)

	c.close()

try:
	print "creating table %s..."%table
	create_table()
except sqlite3.OperationalError,e:
	if str(e) != 'table %s already exists'%table:
		print "error creating table!"
		exit(1)
	print "table %s already exists."%table

def insert_bibtex_file(collection_name,file):
	''' Extract info from bibtex file and insert into sqlite3 database
	collection_name: string key to associate with each entry in the file
	file:  a file object
	'''

	d = bibparse.parse_bib(collection_name,file)

	c = conn.cursor()
	
	for entry in d:
		rowvals = []
		for k in columns:
			try:
				v = entry.data[k]
			except KeyError:
				v = "Empty"
			rowvals.append(v)

		row = ','.join(['"'+r+'"' for r in rowvals])
		types = ','.join(['?' for k in columns])
		insstr = ''' insert into %s values (%s) '''%(table,row)
		print insstr
		try:
			c.execute(insstr)
		except sqlite3.OperationalError:
			# Deliberately ignoring a bug
			# Insert fails when there's an umlaut in a name
			# Sanitizing is necessary to deal with the " or ' that appears
			pass
		
	conn.commit()
	c.close()

def get_collections():
	''' Return list of collections from database '''
	print 'fetching collections'
	c = conn.cursor()

	q = 'select distinct Collection from %s order by Collection asc'%table
	c.execute(q)

	collections = []
	for r in c:
		collections.append(str(r[0]))
	return collections

	c.close()

def search(where_clause):
	''' Search database restricted to where_clause '''
	c = conn.cursor()

	q = 'select * from %s where %s '%(table,where_clause)
	print q
	c.execute(q)

	results = []
	for r in c:
		results.append([str(x) for x in r])
	
	return results

if __name__=='__main__':
	collections = get_collections()
	c = collections[0]

	wc = 'Collection = "%s"'%c

	results = search(wc)

	print results


