#!/usr/bin/env python
'''
AY250 HW6
Image search and manipulation GUI using Traits
Author: Bryan Steinbach
'''


import urllib

import wx
import numpy as np
from enthought.traits.api import HasTraits,Str,Float,Button,Instance
from enthought.traits.ui.api import View,Item,ButtonEditor,VSplit,HSplit
from yahoo.search.image import ImageSearch
from PIL import Image
from mpl_figure_editor import MPLFigureEditor
from matplotlib.figure import Figure
import pylab

def yahoosearch(query):
	''' Yahoo searches for an image and returns a URL, or an empty string if something goes wrong '''
	try:
		search = ImageSearch(app_id="junk",query=query)
		search.results = 1
		for res in search.parse_results():
			return res.Url
	except:
		print "Image searched failed!"
		pass

	return ""

def fetchlink(url):
	''' Fetch file from link and return path to local file '''
	return urllib.urlretrieve(url)[0]

yuvmat = np.array([[0.299,0.587,0.114],[-0.14713,-0.28886,0.436],[0.615,-0.51499,-0.10001]])		# source: wikipedia YUV article
yuvmatinv = np.linalg.inv(yuvmat)
print yuvmatinv

class ImgSearch(HasTraits):
	# Model
	query = Str("Enter search")
	run_query = Button()
	url = Str

	refresh = Button()
	rotate90 = Button()
	rotate_color = Button()
	rgb_to_yuv = Button()
	yuv_to_rgb = Button()

	query = "red star winter orbit"

	figure = Instance(Figure,())

	# Controller - button callbacks
	def _refresh_fired(self):
		''' Reload raw image from disk '''
		self.load_image()
		self.draw_image()

	def _rotate90_fired(self):
		''' Rotate image 90 degrees clockwise '''
		self.ima = pylab.rot90(self.ima)
		self.draw_image()

	def _rgb_to_yuv_fired(self):
		''' Show YUV as RGB '''
		self.color_transform(yuvmat)
		self.draw_image()

	def _yuv_to_rgb_fired(self):
		''' Show RGB as YUV '''
		self.color_transform(yuvmatinv)
		self.draw_image()

	def _rotate_color_fired(self):
		''' Advance color space RGB -> BRG '''
		x = np.zeros_like(self.ima)
		r,g,b = self.ima[:,:,0],self.ima[:,:,1],self.ima[:,:,2]
		x[:,:,0] = b
		x[:,:,1] = r
		x[:,:,2] = g
		self.ima = x
		self.draw_image()


	def _run_query_fired(self):
		''' Search for, download and display image from query text'''
		self.fetch_image()
		self.load_image()
		self.draw_image()

	# Controller helper methods
	def draw_image(self):
		''' Redraw image on canvas '''
		self.figure.axes[0].images=[]
		self.figure.axes[0].imshow(self.ima,aspect='auto',interpolation='nearest')
		wx.CallAfter(self.figure.canvas.draw)

	def load_image(self):
		''' Load image from disk '''
		im = Image.open(self.localfile)
		self.ima = np.asarray(im)/256.0
		print "ima.shape: ",self.ima.shape


	def fetch_image(self):
		''' Search for and download image '''
		print "query: ",self.query
		self.url = yahoosearch(self.query)
		print "url: ",self.url
		self.localfile = fetchlink(self.url)
		print "localfile: ",self.localfile

	def color_transform(self,mat):
		''' Linear colorspace transform '''
		x = np.zeros_like(self.ima)
		for i in range(self.ima.shape[0]):
			for j in range(self.ima.shape[1]):
				x[i,j,:] = np.dot(mat,self.ima[i,j,:])
		self.ima = x

		
	# View
	qi = Item('query',label='Input',width=250)
	rqi = Item('run_query',show_label=False)
	figi = Item('figure',editor=MPLFigureEditor(),show_label=False)
	urli = Item('url',label='Image URL')

	refi = Item('refresh',show_label=False)
	rot90i = Item('rotate90',show_label=False)
	rgbyuvi = Item('rgb_to_yuv',show_label=False)
	yuvrgbi = Item('yuv_to_rgb',show_label=False)
	rci = Item('rotate_color',show_label=False)

	view = View(HSplit(qi,rqi),urli,figi,HSplit(refi,rot90i,rci,rgbyuvi,yuvrgbi),
			width=400,height=300,resizable=True)

	def __init__(self):
		super(ImgSearch,self).__init__()
		self.figure.add_subplot(111)


c = ImgSearch()
c.configure_traits()

