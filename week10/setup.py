from distutils.core import setup,Extension

lms = Extension('libmidstep',['midstep.c'],extra_compile_args=['-std=c99'])

setup(name='midstep',version='1.0',py_modules=['midstep','midstepwrap'],ext_modules=[lms])
