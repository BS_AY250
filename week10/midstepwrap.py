
import numpy as np
import ctypes

_midstep = ctypes.CDLL('libmidstep.so')
_midstep.midstep.restype = ctypes.c_int

array_type = ctypes.POINTER(ctypes.c_double)
_midstep.midstep.argtypes = [array_type,ctypes.c_int]

def midstep(x):
	return _midstep.midstep(x.ctypes.data_as(array_type),len(x))


