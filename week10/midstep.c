#include <stdio.h>
#include <stdlib.h>

/* Bloom's midstep function */

int midstep(const int *step_vector, const int n)
{
	float sum=0.,half;
	for(int i=0;i<n;i++)
		sum += (float)step_vector[i];

	half = sum*0.5;
	sum=0.0;
	for(int i=0;i<n;i++) {
		sum += (float)step_vector[i];
		if(sum >= half)
			return i;
	}
	return n-1;
}

#define N 5
int main(int argc, char **argv)
{
	int s[N] = {1,2,3,4,5};

	printf("Midstep: %d\n",midstep(s,N));
}
