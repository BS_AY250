
import numpy as np

def midstep(v):
	''' Find the index of the step that crosses the halfway point of the total distance travelled '''
	cv = np.cumsum(v)
	return np.searchsorted(cv,cv[-1]/2.0)

