AY250 HW10
Wrapping C code and comparing speed to python
Author: Bryan Steinbach

midstep.py:  Python implementation of midstep function
midstep.c, midstepwrap.py:  C implementation of midstep function and python wrapper
test/testmidstep.py:  Test and compare the two versions, benchmark and unittest

Instructions:
python setup.py build
python setup.py install --home=$HOME
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/lib/python python test/testmidwrap.py
