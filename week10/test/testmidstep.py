
from time import time
import unittest

import numpy as np

import midstep
import midstepwrap

class TestMidstep(unittest.TestCase):
	'''unittest example'''
	def setUp(self):
		self.x = np.random.random_integers(0,10,100)
	
	def testCvsPy(self):
		v0 = midstep.midstep(self.x)
		v1 = midstepwrap.midstep(self.x)
		self.assertEqual(v0,v1)


def test_timing():
	''' Benchmarking '''
	n = 10**7
	test_array = np.random.random_integers(0,10,n)

	t0 = time()

	v0 = midstep.midstep(test_array)

	t1 = time()

	v1 = midstepwrap.midstep(test_array)

	t2 = time()


	dt0 = t1-t0
	dt1 = t2-t1


	print "results: "
	print "python ",v0
	print "C ",v1
	print ""
	print "timing (s): "
	print "python ",dt0
	print "C ",dt1

	if v0 != v1:
		print "Error, the two implementations do not agree!"
		exit(1)


if __name__=='__main__':
	test_timing()
	unittest.main()
