'''
AY250 HW5 A
Build sqlite3 table of polls
Author: Bryan Steinbach
'''

import os
import sqlite3

rawfn = 'senate_polls.raw'
tablefn = 'poll'

if not os.path.exists(rawfn):
	print "Error, missing file %s"%rawfn
	exit(1)

if os.path.exists(tablefn):
	os.remove(tablefn)

conn = sqlite3.connect('poll')

c = conn.cursor()

months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

c.execute(''' create table polls
		(state text, gop int, dem int, ind int, date1 text, date2 text, pollster text)''')

def format_date(month,day):
	moni = months.index(month)+1
	mons = str(moni)
	if len(mons) == 1:
		mons = '0'+mons
	
	return '2010-'+mons+'-'+day

with open(rawfn) as f:
	for line in f.readlines():
		words = line.strip().split()
		state,gop,dem,ind,mon1,day1,mon2,day2 = words[:8]

		date1 = format_date(mon1,day1)
		date2 = format_date(mon2,day2)

		pollster = ' '.join(words[8:])
		c.execute(''' insert into polls
				values (?,?,?,?,?,?,?) ''',(state,gop,dem,ind,date1,date2,pollster))

conn.commit()

# sanity check
c.execute('select * from polls order by date1 desc')
for i in range(10):
	print c.next()

c.close()
