'''
AY250 HW5 C
Build sqlite3 table of names and pictures
Author: Bryan Steinbach
'''

import os
import sqlite3

rawfn = 'candidate_names.txt'
tablefn = 'poll'

base_url = 'http://astro.berkeley.edu/~amorgan/candidates/'
ext = '.gif'

if not os.path.exists(rawfn):
	print "Error, missing file %s"%rawfn
	exit(1)

conn = sqlite3.connect('poll')

c = conn.cursor()

c.execute(''' create table pics
		(name text, url text) ''')

with open(rawfn) as f:
	f.readline()		# Skip first line
	for line in f.readlines():
		words = line.strip().split(',')
		state,dem,gop,ind,incpar = words
		names = [x.strip() for x in (dem,gop,ind) if x.strip() != '']

		for name in names:
			c.execute(''' insert into pics 
					values (?,?) ''',(name,base_url+name+ext))

conn.commit()

# sanity check
c.execute('select * from pics order by name')
for i in range(10):
	print c.next()

c.close()
