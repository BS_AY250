'''
AY250 HW5 B
Build sqlite3 table of states and names
Author: Bryan Steinbach
'''

import os
import sqlite3

rawfn = 'candidate_names.txt'
tablefn = 'poll'

if not os.path.exists(rawfn):
	print "Error, missing file %s"%rawfn
	exit(1)

conn = sqlite3.connect('poll')

c = conn.cursor()

c.execute(''' create table names 
		(state text, gop text, dem text, ind text, incpar text)''')

with open(rawfn) as f:
	f.readline()		# Skip first line
	for line in f.readlines():
		words = line.strip().split(',')
		state,dem,gop,ind,incpar = words
		dem,gop,ind,incpar = [x.strip() for x in (dem,gop,ind,incpar)]
		c.execute(''' insert into names 
				values (?,?,?,?,?) ''',(state,gop,dem,ind,incpar))

conn.commit()

# sanity check
c.execute('select * from names order by gop')
for i in range(10):
	print c.next()

c.close()
