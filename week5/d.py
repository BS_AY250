'''
AY250 HW5 D
Show web page of polling data from one state
Author: Bryan Steinbach
'''

import sys
import os
import sqlite3
import webbrowser

import numpy as np

tablefn = 'poll'

conn = sqlite3.connect(tablefn)


# sanity check
#c.execute('select * from polls where state = "CA"')

def getpic(name):
	''' Fetch image URL from candidate's name '''
	if name == '': return ''
	c = conn.cursor()
	c.execute(''' select url from pics where name = '%s' '''%name)
	(url,) = c.next()
	c.close()

	return url

def retrieve_state(state):
	''' Fetch information about state and render to web page '''
	c = conn.cursor()
	c.execute('''select polls.state,polls.gop,polls.dem,polls.ind,polls.date1,polls.pollster,names.gop,names.dem,names.ind,names.incpar
			from polls,names,pics where polls.state = '%s' and names.state = '%s' order by polls.date1 desc limit 1'''%(state,state))

	state,gopfrac,demfrac,indfrac,date,pollster,gopname,demname,indname,incparname = c.next()

	c.close()

	goppic = getpic(gopname)
	dempic = getpic(demname)
	indpic = getpic(indname)

	fracs = np.array([gopfrac,demfrac,indfrac])
	partys = ['Republican','Democrat','Independent']

	favi = np.argmax(fracs)
	favp = partys[favi]

	fn = state+'.html'
	with open(fn,'w') as f:
		print>>f,"<html>"
		print>>f,"<body>"
		print>>f,"<h1>State of "+state+" polling</h1>"

		if gopname:
			print>>f,"<h2>Republican candidate: ",gopname,"</h2>"
			print>>f,"<img src='"+goppic+"'></img>"
			print>>f,"<p>"
			print>>f,"Polling: ",gopfrac

			print>>f,"<p><p>"

		if demname:
			print>>f,"<h2>Democratic candidate: ",demname,"</h2>"
			print>>f,"<img src='"+dempic+"'></img>"
			print>>f,"<p>"
			print>>f,"Polling: ",demfrac

			print>>f,"<p><p>"

		if indname:
			print>>f,"<h2>Independent candidate: ",indname,"</h2>"
			print>>f,"<img src='"+indpic+"'></img>"
			print>>f,"<p>"
			print>>f,"Polling: ",indfrac

			print>>f,"<p><p>"

		print>>f,"The incumbent party is "+incparname+"."
		if favp != incparname:
			print>>f,"We expect that to change to %s this election."%favp
		else:
			print>>f,"We don't expect that to change this election."

		print>>f,"</body>"
		print>>f,"</html>"
	
	webbrowser.open(fn)


try:
	state = sys.argv[1]
except IndexError:
	print "usage: python d.py <2 char state abbreviation>"
	exit(1)
retrieve_state(state)
