'''
AY250 HW5 G
Insert another record into the polls table
Author: Bryan Steinbach
'''

import sys
import os
import sqlite3

tablefn = 'poll'

months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

def format_date(month,day):
	moni = months.index(month)+1
	mons = str(moni)
	if len(mons) == 1:
		mons = '0'+mons
	
	return '2010-'+mons+'-'+day

words = sys.argv[1:]
if len(words) < 9:
	print "usage: python g.py <state> <gop frac> <dem frac> <ind frac> <month1> <day1> <month2> <day2> <pollster>"
	exit(1)

state,gop,dem,ind,mon1,day1,mon2,day2 = words[:8]
pollster = ' '.join(words[8:])

date1 = format_date(mon1,day1)
date2 = format_date(mon2,day2)

conn = sqlite3.connect(tablefn)

c = conn.cursor()

c.execute(''' insert into polls
		values (?,?,?,?,?,?,?) ''',(state,gop,dem,ind,date1,date2,pollster))

conn.commit()

c.close()
