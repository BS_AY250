'''
AY250 HW5 F
Plot polling data timeseries for one state
Author: Bryan Steinbach
'''

import sys
import os
import sqlite3
import webbrowser

import numpy as np

import pylab as pl

tablefn = 'poll'

conn = sqlite3.connect(tablefn)

def retrieve_state(state):
	c = conn.cursor()
	c.execute('''select polls.gop,polls.dem,polls.ind,polls.date1
			from polls,names where polls.state = ? and names.state = ? order by polls.date1 asc''',(state,state))

	gopfracs,demfracs,indfracs,dates = [],[],[],[]
	for r in c:
		gopfrac,demfrac,indfrac,date = r
		gopfracs.append(gopfrac)
		demfracs.append(demfrac)
		indfracs.append(indfrac)
		dates.append(date)

	c.close()

	c = conn.cursor()
	c.execute('''select gop,dem,ind from names where state = ? ''',(state,))

	gopname,demname,indname = c.next()
	c.close()

	dates = np.array(dates)
	dates = pl.datestr2num(dates)
	dates = (1 + dates/365.242199 - 2010)*365.242199


	if gopname:
		pl.plot(dates,gopfracs,color='red',label=gopname)
	if demname:
		pl.plot(dates,demfracs,color='blue',label=demname)
	if indname:
		pl.plot(dates,indfracs,color='green',label=indname)
	
	pl.legend()
	pl.grid()
	pl.xlabel('Date (Day of 2010)')
	pl.ylabel('Percent polling for candidate')

	pl.title('Polling data for '+state)
	pl.ylim(0,100)

	pl.savefig(state+'.png')

	pl.show()


try:
	state = sys.argv[1]
except IndexError:
	print "usage: python f.py <2 char state abbreviation>"
	exit(1)

retrieve_state(state)

