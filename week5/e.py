'''
AY250 HW5 E
Show seats lost by democrats
Author: Bryan Steinbach
'''

import os
import sqlite3
import webbrowser

import numpy as np

tablefn = 'poll'

conn = sqlite3.connect(tablefn)

def retrieve_state(state):
	c = conn.cursor()
	c.execute('''select polls.gop,polls.dem,polls.ind,names.incpar
			from polls,names where polls.state = '%s' and names.state = '%s' order by polls.date1 desc limit 1'''%(state,state))

	gopfrac,demfrac,indfrac,incpar = c.next()

	c.close()

	fracs = np.array([gopfrac,demfrac,indfrac])
	partys = ['Republican','Democrat','Independent']

	favi = np.argmax(fracs)
	favp = partys[favi]

	return incpar,favp

def get_states():
	c = conn.cursor()
	c.execute(''' select distinct state from polls ''')
	states = []
	for s in c: states.append(s[0])
	c.close()

	return states

states = get_states()

incpars,newpars = [],[]
for state in states:
	incpar, newpar = retrieve_state(state)

	incpars.append(incpar)
	newpars.append(newpar)

def countseats(parties):
	gop = 0
	dem = 0
	ind = 0

	for p in parties:
		if p == 'Republican': gop += 1
		elif p == 'Democrat': dem += 1
		elif p == 'Independent': ind += 1
		else:
			print "Unknown party: ",p

	return gop,dem,ind

gop0,dem0,ind0 = countseats(incpars)
gop1,dem1,ind1 = countseats(newpars)

lost_seats = dem0-dem1

print "Seats lost by democrats: ",lost_seats
