#!/usr/bin/env python

import sys
import os

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab

from matplotlib.ticker import MultipleLocator


googlefn = 'google_data.csv'
nyfn = 'ny_temps.csv'
yahoofn = 'yahoo_data.csv'

def checkexists(fn):
	if not os.path.exists(fn):
		print "%s not found"%fn
		sys.exit(2)

checkexists(googlefn)
checkexists(nyfn)
checkexists(yahoofn)

def loadcsv(fn):
	return np.loadtxt(fn,skiprows=1,unpack=True,delimiter=',')

google_x , google_y = loadcsv(googlefn)
ny_x , ny_y = loadcsv(nyfn)
yahoo_x ,yahoo_y = loadcsv(yahoofn)


lg = pylab.plot(google_x,google_y,color='blue',label='Google Stock Value',linewidth=1.5)
ly = pylab.plot(yahoo_x,yahoo_y,color='purple',label='Yahoo Stock Value',linewidth=1.5)


pylab.xlabel('Date (MJD)')
pylab.ylabel('Value (Dollars)')
pylab.suptitle('New York Temperature, Google, and Yahoo!',fontsize=20)

minorLocator = MultipleLocator(20)
ax = pylab.gca()
ax.yaxis.set_minor_locator(minorLocator)


ax2 = pylab.twinx()
lny = pylab.plot(ny_x,ny_y,color='red',linestyle='--',label='NY Mon. High Temp',linewidth=1.5)
pylab.ylim(-150,100)

pylab.legend((ly,lg,lny),('Yahoo! Stock Value','Google Stock Value','NY Mon. High Temp'),loc=6,frameon=False,prop={'size':10})

minorLocator = MultipleLocator(200)
ax.xaxis.set_minor_locator(minorLocator)
minorLocator = MultipleLocator(10)
ax2.yaxis.set_minor_locator(minorLocator)

pylab.xlim(48800,55600)

pylab.ylabel('Temperature ($^\circ$F)')

pylab.savefig('student-stocks.png',dpi=200)
