#!/usr/bin/env python

'''
AY250 HW4 Problem 3
Interactive Brush Plotting
Author:  Bryan Steinbach
'''

import sys
import os

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab


#### Brushing library

class Box:
	''' Draw rectangles on plots
	Thin wrapper over matplotlib rectangle '''
	def __init__(self,axes,ax,ay):
		self.axes = axes
		self.rect = matplotlib.patches.Rectangle((ax,ay),0,0,axes=ax,fill=False)
		self.p0 = (ax,ay)
		self.wh = (0,0)
		axes.add_patch(self.rect)
	def set(self,bx,by):
		''' Set the far corner of the box '''
		self.wh = (bx-self.p0[0]),(by-self.p0[1])
		self.rect.set_width(self.wh[0])
		self.rect.set_height(self.wh[1])
	def contains(self,axes,x,y):
		''' Check if box contains a point with coordinates axes,x,y '''
		return self.axes == axes and self.rect.get_bbox().contains(x,y)
	def remove(self):
		return self.rect.remove()

class BrushPlot:
	def __init__(self,data,plot_pairs):
		''' data:  dictionary with key the plot label and value the column
		plot_pairs:  label of columns to combine in subplot '''
		self.data = data
		self.plot_pairs = plot_pairs

		self.fig = plt.figure()
		self.axes = []
		self.plots = []
		n = len(plot_pairs)

		# Live selection boxes
		self.boxes = []
		# Store box here while mouse button pressed
		self.current_box = None

		self.connect()

		# Create all the plots in a single column
		for i,(xl,yl) in enumerate(plot_pairs):
			ax = self.fig.add_subplot(n*100 + 10 + i+1)
			self.axes.append(ax)

		for ax,(xl,yl) in zip(self.axes,plot_pairs):
			s = ax.scatter(data[xl],data[yl],linewidths=0)
			self.plots.append(s)
			ax.set_xlabel(xl)
			ax.set_ylabel(yl)

		# The brushing looks less cool with black edges.  set_color sets face&edge color
		for s in self.plots:
			s.set_color('blue')


	def connect(self):
		''' Register input events with instance methods '''
		self.cidpress = self.fig.canvas.mpl_connect('button_press_event',self.on_press)
		self.cidrelease = self.fig.canvas.mpl_connect('button_release_event',self.on_release)
		self.cidmotion = self.fig.canvas.mpl_connect('motion_notify_event',self.on_motion)
		self.cidkeypress = self.fig.canvas.mpl_connect('key_press_event',self.on_key_press)
	
	def get_points_in_box(self,box):
		''' Return bitmask vector which is True for selected points for the specified box '''
		axes = box.axes
		i = self.axes.index(axes)
		xl,yl = plot_pairs[i]

		mask_vector = [box.contains(axes,x,y) for x,y in zip(self.data[xl],self.data[yl])]

		return mask_vector

	def get_points_in_boxes(self):
		''' Return bitmask vector which is True for selected points in all boxes '''
		mask_vector = [False for x in self.data.values()[0]]

		for box in self.boxes:
			box_mask = self.get_points_in_box(box)
			mask_vector = [x or y for x,y in zip(mask_vector,box_mask)]

		return mask_vector

	def on_key_press(self,event):
		''' Delete a box if d is pressed with mouse over '''
		axes = event.inaxes
		if not axes in self.axes: return

		if event.key == 'd':
			ax,ay = event.xdata,event.ydata

			newboxes = []
			for b in self.boxes:
				if b.contains(axes,ax,ay):
					b.remove()
				else:
					newboxes.append(b)
			self.boxes = newboxes

			self.recolor()

	def on_press(self,event):
		''' Press mouse button to make a new box '''
		axes = event.inaxes
		if not axes in self.axes: return

		ax,ay = event.xdata,event.ydata

		self.current_box = Box(axes,ax,ay)
		self.boxes.append(self.current_box)

	def on_motion(self,event):
		''' Drag mouse to move far corner of box '''
		if self.current_box is None: return
		axes = event.inaxes
		if not axes in self.axes: return

		bx,by = event.xdata,event.ydata

		self.current_box.set(bx,by)

		self.fig.canvas.draw()
	
	def recolor(self):
		''' After selecting, reset the colors of all the points to reflect selection '''
		mask_points = self.get_points_in_boxes()
		def colorgen(x):
			if x: return (0,0,1,0.05)	# RGBA.  Magic values I liked.
			else: return (0,0,1,1)
		colors = np.array([colorgen(x) for x in mask_points])

		for s in self.plots:
			s.set_color(colors)

		self.fig.canvas.draw()

	def on_release(self,event):
		''' Once the mouse button is released, stop dragging the box corner, and recolor '''
		axes = event.inaxes
		if not axes in self.axes: return

		self.current_box = None

		self.recolor()


#### Application code to handle hormel.csv data file
fn = 'hormel.csv'
if not os.path.exists(fn):
	print "error - %s does not exist"%fn
	exit(2)

with open(fn) as f:
	labels = f.readline().rstrip().split(',')

data = np.loadtxt(fn,skiprows=1,converters={0:pylab.datestr2num},unpack=True,delimiter=',')

data[0] = 1+data[0]/365.242199		# Convert from days to years, amortizing over leap years.

d = {}
for label,xs in zip(labels,data):
	d[label] = xs

# Reverse the order because I typed them in backwards from the example, not that it matters.
plot_pairs = [(y,x) for x,y in [('Open','Date'),('Close','Open'),('Volume','Date'),('High','Low')]]	

#Matplotlib 1.0 workaround - hold on to a reference to my figures, or they'll get GC'd away.
bp = BrushPlot(d,plot_pairs)

plt.show()
