#!/usr/bin/env python
'''
AY250 HW4 Problem 1
Make a nice plot
Author:  Bryan Steinbach
'''

import os

import numpy as np
import pylab

fn = 'starcamdata-20100828.npy'
if not os.path.exists(fn):
	print "Couldn't find %s"%fn
	exit()

azs,dazs,model_dazs,els,dels,model_dels = np.load(fn)

nsource = 15
total_time = 6.1
allowed_params = 'npae ca ia ie tf ta te'

axfs = 10
def azelplot():
	pylab.figure()

	# Matplotlib doesn't have intelligent layout, so we have to manually pull in the subplots to make room for title and legend
	pylab.subplots_adjust(bottom=0.16,top=0.89,wspace=0.25,hspace=0.25)

	pylab.title('Star camera pointing offsets')

	pylab.subplot(221)
	pylab.scatter(azs,dazs,facecolor='white')
	pylab.scatter(azs,model_dazs,marker='+')
#	pylab.xlabel('Az (degrees)',fontsize=axfs)
	pylab.ylabel('$\Delta$Az (arcminutes)',fontsize=axfs)
	pylab.grid()

	pylab.subplot(222)
	pylab.scatter(els,dazs,facecolor='white')
	pylab.scatter(els,model_dazs,marker='+')
#	pylab.xlabel('El (degrees)',fontsize=axfs)
#	pylab.ylabel('$\Delta$Az (arcminutes)',fontsize=axfs)
	pylab.grid()

	pylab.subplot(223)
	pylab.scatter(azs,dels,facecolor='white')
	pylab.scatter(azs,model_dels,marker='+')
	pylab.xlabel('Az (degrees)',fontsize=axfs)
	pylab.ylabel('$\Delta$El (arcminutes)',fontsize=axfs)
	pylab.grid()

	pylab.subplot(224)
	l1 = pylab.scatter(els,dels,facecolor='white')
	l2 = pylab.scatter(els,model_dels,marker='+')
	pylab.xlabel('El (degrees)',fontsize=axfs)
#	pylab.ylabel('$\Delta$El (arcminutes)',fontsize=axfs)
	pylab.grid()

	pylab.suptitle('Star Camera Pointing Model  8/28/10  %d stars  %.1f hours\nModel free parameters: %s'%(nsource,total_time,allowed_params),fontsize=14)

	pylab.figlegend((l1,l2),('Measured star position - boresight','Model'),'lower center',prop={'size':10})


	pylab.savefig('starcam-20100828.png',figsize=((10,8)),dpi=250)


azelplot()
#pylab.show()
