# Run the script for problem2 over all input files

import os
import sys
import glob

indir = 'sound_files'
outdir = 'output'

files = glob.glob(os.path.join(indir,'*.aif'))
files.sort()

for infn in files:
	basedir,justfn = os.path.split(infn)
	fn_noext,ext = os.path.splitext(justfn)
	outfn = os.path.join(outdir,fn_noext)+'.txt'
	cmd = 'python problem2.py %s > %s'%(infn,outfn)
	print cmd
	os.system(cmd)
