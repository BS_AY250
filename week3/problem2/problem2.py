#!/usr/bin/env python
'''
AY250 HW3 Problem 2
Note extraction
Author:  Bryaqn Steinbach

Strategy:
   * Extract left channel from stereo
   * Compute power spectrum for whole timestream
   * Find and fit to peak frequencies
   * Discard spurious peaks from chorus which are too far from a properly tuned note
   * Pick fundamentals to be peaks which can't be explained as harmonics of a lower note
'''

import sys
import aifc

import pylab as pl
import numpy as np
from scipy import signal
from numpy import pi

try:
	fn = sys.argv[1]
except IndexError:
	print "Usage: python problem2.py sound_files/1.aif"
	exit(2)

sf = aifc.open(fn)

nchannels = sf.getnchannels()
sampwidth = sf.getsampwidth()
framerate = sf.getframerate()
nframes = sf.getnframes()

if nchannels != 2:
	print "Expected stereo sound file"
	exit(1)
if sampwidth != 2:
	print "Expected 16b samples"
	exit(1)

# I played with the sign and endianness until the histogram showed some nice, non maximal entropy pattern
data = np.fromstring(sf.readframes(nframes),dtype=np.int16).byteswap()/2.0**15

left = data[::nchannels]
right = data[1::nchannels]

if left.size != right.size:
	print "left/right channels don't have the same number of samples, is this really stereo?"
	exit(1)

if left.size != nframes:
	print "Number of samples in left channel doesn't match frame count"
	exit(1)

pl.hist(left,bins=30)
pl.title('Signal histogram')
pl.savefig('signal_histogram.png')
pl.close()

pl.plot(left)
pl.plot(right)
pl.ylabel('Signal counts')
pl.xlabel('Sample number')
pl.title('Signal timestreams')
pl.savefig('signal_ts.png')
pl.close()


# Fit a peak around index i
def peakfit(x,y,i):
	ii = [i-1,i,i+1]
	a,b,c = np.polyfit(x[ii],y[ii],2)
	xmax = -b/(2*a)
	ymax = np.polyval([a,b,c],xmax)
	return xmax,ymax

# Fit to the highest peak
def findmax(x,y):
	xi = np.argmax(y)
	return peakfit(x,y,xi)

def findpeaks(x,y):
	threshold = 0.03		# -15dB from loudest sound, to cut down on junk peaks
	inpeak = y > threshold

	# Split the data into a list that brackets peaks
	blocks = []
	peak_start = None
	for i in range(inpeak.size):
		if inpeak[i] == True and peak_start == None:
			peak_start = i
		if inpeak[i] == False and peak_start != None:
			blocks.append((peak_start,i))
			peak_start = None
	
	peaks = []
	for start,end in blocks:
		xc = x[start-1:end+1]
		yc = y[start-1:end+1]
		peak = findmax(xc,yc)
		peaks.append(peak)

	return peaks

def integer_distance(x):
	''' Signed distance between a float and the nearest integer '''
	return ((x + 0.5) % 1.0) - 0.5


def clean_peaks(peak_freqs):
	''' Remove peaks for things which are not notes '''

	ok = []
	for f in peak_freqs:
		note_num = 12*np.log2(f/ref_note)
		if abs(integer_distance(note_num)) < 0.05:
			ok.append(f)
	return ok

def find_fundamentals(peak_freqs):
	'''
	find_fundamentals(peak_freqs)
	peak_freqs should be in sorted order
	no amplitude information is used

	The harmonic structure of the note is dependent on the note
	F3/A4 from the Pop Organ have considerably different harmonic powers
	That means there isn't enough information in the supplied files to disentangle when two notes have harmonic overlapping harmonic
	So, we'll have to assume the assignment writer was generous, and didn't overlap harmonics with fundamentals
	Under this assumption we can assign peaks to notes using a sieving process
	'''

	notes = []
	for f in peak_freqs:

		# Try to explain this peak frequency as the harmonic of another note
		is_harmonic = False
		for note in notes:
			ratio = f / note
			err = abs(integer_distance(ratio))
			harm_number = round(ratio)
			if err/harm_number < 0.025: # Half a half step
				is_harmonic = True
				break

		if not is_harmonic:
			notes.append(f)

	return notes

def note_to_name(note_freq):
	note_num = (12*(np.log2(note_freq/ref_note) + 4))
	
	err = integer_distance(note_num)

	note_num = int(round(note_num))

	oct = note_num / 12
	sub = note_num % 12

	return note_base[sub]+str(oct),err

# Octave starts at C
note_base = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B']
if len(note_base) != 12: print "Length of note_base not 12"
ref_note = 220*2**(3.0/12.0) # A3 -> C4

p,f = pl.psd(left,Fs=framerate,NFFT=nframes/16)
pl.xlim(0,2000)
pl.title('Signal power spectrum')
pl.savefig('signal_psd.png')

fm,pm = findmax(f,p)
p /= pm

peaks = findpeaks(f,p)

peak_freqs = [peak[0] for peak in peaks]

peak_freqs = clean_peaks(peak_freqs)


notes = find_fundamentals(peak_freqs)

print 'pitch\tcents error'
for note in notes:
	name,err = note_to_name(note)
	print '%s\t%f'%(name,err)
