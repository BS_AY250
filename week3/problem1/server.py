
import SimpleXMLRPCServer

import numpy as np
from scipy import signal

import image

image.outdir = 'server_data'

class ImageManipulation(object):
	def __init__(self):
		self.log_index = 0

	def __server_method(self,imagestr,fxchan):
		''' Generic server method for processing an image, marshalling, saving data products '''
		x = image.unmarshal(imagestr)
		image.save_image('raw%d.jpg'%(self.log_index),x)

		y = image.process_image(x,fxchan)

		image.save_image('final%d.jpg'%(self.log_index),y)

		self.log_index += 1

		return image.marshal(y)

	def double(self,imagestr):
		''' Double size of an image by assuming image is band limited '''
		def fxchan(x):
			x = signal.resample(x,x.shape[0]*2,axis=0,window='boxcar')
			x = signal.resample(x,x.shape[1]*2,axis=1,window='boxcar')
			return x

		return self.__server_method(imagestr,fxchan)

	def rot180(self,imagestr):
		''' Rotate image by 180 degrees '''
		def fxchan(x):
			return x[::-1]

		return self.__server_method(imagestr,fxchan)

	def invert_color(self,imagestr):
		''' Flip intensities '''
		def fxchan(x):
			return 255 - x

		return self.__server_method(imagestr,fxchan)

host,port = "localhost",5020

server = SimpleXMLRPCServer.SimpleXMLRPCServer((host,port),allow_none=True)
server.register_instance(ImageManipulation())
server.register_multicall_functions()
server.register_introspection_functions()

print "Server running on ",host,port
server.serve_forever()


