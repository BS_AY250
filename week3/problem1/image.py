
from os.path import join as jp
import cPickle as pickle

from PIL import Image
import numpy as np

outdir = ''

def load_image(fn):
	''' Load an image as a numpy array '''
	im = Image.open(fn)
	arr = np.asarray(im)
	return arr

def save_image(fn,arr):
	''' Save an array as an image '''
	pim = Image.fromarray(arr)
	fn = jp(outdir,fn)
	pim.save(fn)

def process_image(image,fxchan):
	''' Process image with fxchan, applying f to each channel for RGB '''

	if len(image.shape) == 2:		# Grey scale image
		final_image = fxchan(image)
	elif len(image.shape) == 3:		# RGB Color
		r,g,b = image[:,:,0],image[:,:,1],image[:,:,2]
		final_r = fxchan(r)
		final_g = fxchan(g)
		final_b = fxchan(b)
		final_image = np.zeros((final_r.shape[0],final_r.shape[1],3))
		final_image[:,:,0],final_image[:,:,1],final_image[:,:,2] = final_r,final_g,final_b

	final_image = np.array(final_image,dtype=image.dtype)

	return final_image

def marshal(x):
	''' Convert a numpy array into network transmittable format '''
	return pickle.dumps((x.shape,x.dtype,x.tostring()))
def unmarshal(s):
	''' Convert network transmittable format back to numpy array '''
	x = pickle.loads(s)
	shape = x[0]
	t = x[1]
	data = x[2]

	da = np.fromstring(data,dtype=t)

	return da.reshape(shape)
