
import xmlrpclib
import sys

import numpy as np
from scipy import signal

import image

image.outdir = 'client_data'

imgfn = 'halfdome_from_northdome.jpg'

host,port = "localhost",5020

server = xmlrpclib.ServerProxy("http://%s:%d"%(host,port))
available_methods = server.system.listMethods()

print available_methods

print server.system.methodHelp('double')
print server.system.methodHelp('rot180')
print server.system.methodHelp('invert_color')

def wrap_server(f,img):
	''' Run server method f on img, marshalling and unmarshalling the img '''
	return image.unmarshal(f(image.marshal(img)))

''' Methods to undo server methods '''
def halve_image(img):
	''' Inverse of double '''
	def fxchan(x):
		x = signal.resample(x,x.shape[0]/2,axis=0,window='boxcar')
		x = signal.resample(x,x.shape[1]/2,axis=1,window='boxcar')
		return x

	return image.process_image(img,fxchan)

def rot180(img):
	''' Inverse of 180 degree rotation '''
	def fxchan(x):
		return x[::-1]

	return image.process_image(img,fxchan)

def invert_color(img):
	''' Inverse of flipping intensities '''
	def fxchan(x):
		return 255 - x

	return image.process_image(img,fxchan)


index = 0
img = image.load_image(imgfn)

def runtest(srv,cli):
	global index
	image.save_image('raw%d.jpg'%index,img)

	img_mod = wrap_server(srv,img)

	image.save_image('mod%d.jpg'%index,img_mod)

	img_res = cli(img_mod)

	image.save_image('res%d.jpg'%index,img_res)

	index += 1

def runtest_client(cli):
	global index
	image.save_image('raw%d.jpg'%index,img)

	img_res = cli(img)

	image.save_image('res%d.jpg'%index,img_res)
	index += 1

runtest(server.double,halve_image)
runtest(server.rot180,rot180)
runtest(server.invert_color,invert_color)

