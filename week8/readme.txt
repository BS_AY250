AY250 HW8
Author: Bryan Steinbach


Problem 1:  HW solutions are in repo.or.cz:
git clone git://repo.or.cz/BS_AY250

Problem 2:  problem2/changelog.txt - git changelog from a fourier transform spectrometer code I wrote a couple months ago

Problem 3:  Using your solution for week5, because it's nicely librarified.  I've dumped it in week5_soln/.  I've interpreted "add new poll without a format" to mean on the command line with switches whose order doesn't matter, other than the 3rd state going with the 3rd poll_date.

Example:

python problem3.py -b senate2010.sqlite -s AL -p "09/10/10" -r 28 -d 60 -i 0 -w Rasmussen -s AK -r 23 -d 63 -i 37 -p "10/23/10" -w "Dittman Research" -x AL

Generates plots/AL.png

Problem 4:  Checked out bloomdemo as problem4/.  Git is smart enough not to recurse into that subdirectory, so it won't show up the repo.or.cz HW repository, but it will show up in the tarball I email you.
