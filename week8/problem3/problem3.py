#!/usr/bin/env python

'''
AY250 HW8
Argparse / module modification to week5
Author: Bryan Steinbach
'''

import os
import sys

sys.path.append('../..') # Rather than add the HW solutions to my path, 

try:
	from week5_soln import SenatePolls
except ImportError:
	print "Failed to import SenatePolls - is week5_soln in your path?"
	exit(1)

import argparse

parser = argparse.ArgumentParser(description="HW8 interface to HW5")

parser.add_argument('-b', action='store', dest='dbpath', help='Database path')
parser.add_argument('-s', action='append', dest='state', default=[], help='State')
parser.add_argument('-p', action='append', dest='poll_date', default=[], help='Poll date')
parser.add_argument('-r', action='append', dest='rep_pct', default=[], help='Rep pct')
parser.add_argument('-d', action='append', dest='dem_pct', default=[], help='Dem pct')
parser.add_argument('-i', action='append', dest='ind_pct', default=[], help='Ind pct')
parser.add_argument('-w', action='append', dest='pollster', default=[], help='Pollster')
parser.add_argument('-x', action='store', dest='show_state', help='Show updated plot for state')

results = parser.parse_args()

def usage():
	print "All of s,p,r,d,i,w must be present exactly once per state date you enter!"

ns = [len(x) for x in [results.state,results.poll_date,results.rep_pct,results.dem_pct,results.ind_pct,results.pollster]]
n = ns[0]
if not all([ni == n for ni in ns]):
	usage()
	exit(1)

dbpath = results.dbpath
if not dbpath:
	dbpath = '../../week5_soln/senate2010.sqlite'
	print "No database was specified, trying "+dbpath

if not os.path.exists(dbpath):
	print "Database %s not found"%dbpath
else:
	print "Database %s found"%dbpath

for i in range(n):
	print 'Adding database entry from state '+results.state[i]
	SenatePolls.AddNewPoll(dbpath,results.state[i],results.poll_date[i],int(results.rep_pct[i]),int(results.dem_pct[i]),int(results.ind_pct[i]),results.pollster[i])

if results.show_state:
	SenatePolls.PlotPollingData(dbpath,results.show_state)
